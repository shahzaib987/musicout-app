package com.dilbir.android.musicout.util;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by root on 14/12/17.
 */

public class UpdateLocation_Task extends AsyncTask<String, Void, String> {

    Context context;
    UserData userData;
    int user_id;

    public UpdateLocation_Task(Context context, UserData userData, int user_id) {
        this.context = context;
        this.userData = userData;
        this.user_id = user_id;
    }

    @Override
    protected String doInBackground(String... params) {



        //------------  Add Post in the Database  ---------------//

        String addfile_url = "http://mycarolbella.com/UsersDataApp/update_location.php";

        try {




            URL url = new URL(addfile_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            BufferedReader bufferedReader = null;
            String json_string = "";

            String data = URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(this.user_id), "UTF-8") + "&" +
                    URLEncoder.encode("location", "UTF-8") + "=" + URLEncoder.encode(userData.getLocation(), "UTF-8");

            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            os.close();
            InputStream IS = httpURLConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(IS));
            StringBuilder stringBuilder = new StringBuilder();

            while ((json_string = bufferedReader.readLine()) != null) {

                stringBuilder.append(json_string);

            }
            bufferedReader.close();
            IS.close();
            httpURLConnection.disconnect();

            return stringBuilder.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Can't Connect";
    }

    @Override
    protected void onPostExecute(String output) {

       // Toast.makeText(context, "Location Updated", Toast.LENGTH_SHORT).show();

    }
}
